This simple library is meant to authomatize some boring work when using handlebar templates in web pages (client side).
It **requires** _jQuery_ and _Handlebars_ to work.

First of all, include the required libraries and this one after them:
```HTML
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.min.js"></script>
   
   <script src="/path/to/deferredhbloader.js"></script>
```

Then use it this way:
```javascript
    //intialize with the handpoint where your templates are:
    var tplhandler = tplHandler("/path/to/your/templates.html");
    
    //render your data using a template
    tplhandler.getTpl("templatename",function(tpl){
        $(".whereyouwantoputit").append( tpl( yourdata ) )	;
    });
```

The templates need to be in tags with the class _hbltpl_ and an attribute _data-name_ with the name of the template. This way you can have many templates in the same file, avoiding to many ajax calls.
If you want to register a partial add a class _partial_
This is an example of how it may look like:
```HTML
    <div>
      <div class="hbltpl" data-name="author_tpl">
        <h2>{{name}} {{surname}}</h2>
        <p>{{bio}}</p>
      </div>
    
      <div class="hbltpl partial" data-name="book_tpl">
        <h2>{{title}}</h2>
        <h3>{{author}}</h3>
        <p>{{description}}</p>
      </div>
    </div>
```


