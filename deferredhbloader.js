function tplHandler(ep, deferload) {
  
  this._getMyTemplates = function(templates) {
    if (!templates) {
      // called without parameter
      var tpl = this.templates;
    } else {
      var tpl = templates;
    }
    return tpl;
  };

  this._loadtpl = function(templates) {
    //var deferred = Array();
    var me = this;
    for (var i in this.ep) {
      var index = i;
      var cb = function(resp){
          jQuery("body").append(
            '<div id="tplHandlercontainer_'+index+'">' + resp + "</div>"
          );
          var tpl = me._getMyTemplates(templates);
          jQuery("#tplHandlercontainer_"+index)
            .find(".hbltpl")
            .each(function(i, e) {
              var el = jQuery(e);
              var name = el.attr("data-name") || "tpl_" + index;
              if(el.hasClass('partial')){                
                tpl[name] = Handlebars.registerPartial(name, el.html());
              } else {
                tpl[name] = Handlebars.compile(el.html());
              }
              
            });
          jQuery("#tplHandlercontainer_"+index).remove();
          //this.deferred.pop();
      }
      
      this.deferred.push(
        jQuery.get(this.ep[i], cb )
        .fail(function() {
          alert( "error loading template source " );
        } )
        );
      this.loadedEP.push(i);
    }
    this.ep = Array();
    return this.deferred;
  };

  this.getTpl = function(name, cb, templates) {
    var templates = this._getMyTemplates(templates);
    if (name in templates) {
      cb(templates[name]);
    } else {
      jQuery.when.apply(jQuery, this._loadtpl(templates)).then(function() {
        console.log("deferred load required for " + name);
        if (name in templates) {
          cb(templates[name]);
        }else{
          console.log("template " + name + " not found!");
        }
      });
    }
    return this;
  };

  this.addEPs = function(ep) {
    if (ep) {
      if (ep.constructor === Array) {
        this.ep = this.ep.concat(ep);
      } else {
        this.ep = Array(ep);
      }
      if (!deferload) {
        this._loadtpl();
      }
    }
    return this;
  };

  this.init = function(ep, deferload) {
    this.deferred = Array();
    this.loadedEP = Array();
    this.templates = Array();
    deferload = deferload || false;
    if (ep) {
      if (ep.constructor === Array) {
        this.ep = ep;
      } else {
        this.ep = Array(ep);
      }
      if (!deferload) {
        this._loadtpl();
      }
    } else {
      this.ep = Array();
    }
  };

  this.init(ep, deferload);
  return this;
}
